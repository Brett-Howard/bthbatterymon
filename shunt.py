from enum import Enum
from collections import namedtuple
from datetime import datetime
import pickle

shuntSample_t = namedtuple('shuntSample_t', 'timestamp voltage current')
shuntSample_t.__doc__ = 'A data point from a shunt which contains the voltage and current at that point in time.'
shuntSample_t.timestamp.__doc__ = 'The timestamp of the current sample'
shuntSample_t.voltage.__doc__ = 'The voltage at the sampled time'
shuntSample_t.current.__doc__ = 'The current at the sampled time'

class ShuntType(Enum):
    both = 'both'
    charging = 'charging'
    discharging = 'discharging'


class Shunt:
    def __init__(self,
                 name,  # What would you like to call this shunt
                 *,     # star here forces the rest of the parameters to be passed via named argument for readability
                 shunt_val: tuple,  # tuple of maxAmps and MaxMillivolts eg (500,50) for a 500A 50mV shunt.
                 shunt_type: ShuntType,
                 adc_i2c_addr
                 ):
        self.name = name
        self.shunt_val = shunt_val
        self.shunt_type = shunt_type
        self.adc_i2c_addr = adc_i2c_addr
        self.history = []

    def __del__(self):
        ...
        # print("backing up to {0}" .format("." + self.name + " Backup.bin"))
        # self.pickleHistory("." + self.name + " Backup.bin")  #create a hidden backup file on destruction

    def addHistoryValue(self, shuntSample):
        if self.shunt_type == ShuntType.charging:   # if charging type only allow positive currents.
            shuntSample = shuntSample._replace(current = abs(shuntSample.current))
        elif self.shunt_type == ShuntType.discharging:  # if discharging type only allow negative currents
            shuntSample = shuntSample._replace(current = (abs(shuntSample.current) * -1))

        # Now store the value as a named tuple into the history list.
        self.history.append(shuntSample)

    def pickleHistory(self, filename):
        with open(filename, 'wb') as pfile:
            pickle.dump(self.history, pfile)
        pfile.close()

    def unPickleHistory(self, filename):
        with open(filename, 'rb') as pfile:
            self.history = pickle.load(pfile)
        pfile.close()

    def clearHistory(self):
        self.history = []

    def __bool__(self):
        return bool(len(self.history))

    def __len__(self):
        return len(self.history)
