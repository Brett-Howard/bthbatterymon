from shunt import Shunt
from shunt import ShuntType
from shunt import shuntSample_t
from datetime import datetime
import random
import time
import os
from engineering_notation import EngNumber

shunts = \
    [
        Shunt('House Battery',
              shunt_val=(500, 50),  # 500A @ 50mV
              shunt_type=ShuntType('both'),
              adc_i2c_addr=0x50
              ),

        Shunt('Solar Panel',
              shunt_val=(20, 75),  # 20A @ 75mV
              shunt_type=ShuntType('charging'),
              adc_i2c_addr=0x51
              )
    ]

print("\nThe truthyness of the shunt list is currently: {0}".format([bool(x) for x in shunts]))

for i in shunts:
    print()
    print("This shunt is called {0}".format(i.name))
    print("it is of type {0}".format(i.shunt_type))
    print("and it will produce {0} mV/A".format(i.shunt_val[1] / i.shunt_val[0]))

start = time.perf_counter()
for i in range(100000):
    for curShunt in shunts:
        curShunt.addHistoryValue(shuntSample_t(timestamp=datetime.now(),
                                 voltage=random.uniform(12.5, 13.8),
                                 current=random.uniform(-8.0,8.0)))
end = time.perf_counter()

print("\nIt took {0}S to add 100,000 records to both shunts".format(EngNumber(end - start)))

for i in shunts:
    print()
    print("First few records from {0} shunt;".format(i.name))
    for j in i.history[1:10]:
        print("Timestamp = {2} Voltage = {0}V Current = {1}A".format(EngNumber(j.voltage),
                                                                     EngNumber(j.current),
                                                                     j.timestamp))

print("\nThe truthyness of the shunt list is currently: {0}".format([bool(x) for x in shunts]))

for i in shunts:
    print()
    print("Deleting \"{0} Data.bin\"".format(i.name))
    try:
        os.remove(i.name + " Data.bin")
    except OSError:
        pass
    print("Pickling the data into \"{0} Data.bin\"".format(i.name))
    i.pickleHistory(i.name + " Data.bin")
    print("Erasing {0} history".format(i.name))
    i.clearHistory()

print("\nThe truthyness of the shunt list is currently: {0}".format([bool(x) for x in shunts]))
print("There are {0} records in each shunt".format([len(x) for x in shunts]))
print()

for i in shunts:
    print("UnPickling the data from \"{0} Data.bin\"".format(i.name))
    i.unPickleHistory(i.name + " Data.bin")

print("\nThe truthyness of the shunt list is currently: {0}".format([bool(x) for x in shunts]))
print("There are {0} records in each shunt".format([len(x) for x in shunts]))

for i in shunts:
    print()
    print("First few records from {0} shunt;".format(i.name))
    for j in i.history[1:10]:
        print("Timestamp = {2} Voltage = {0}V Current = {1}A".format(EngNumber(j.voltage),
                                                                     EngNumber(j.current),
                                                                     j.timestamp))
